#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h> 
#include <errno.h>   	// Error number definitions
#include <string.h>

//line necessary for strptime to work 
#define __USE_XOPEN
#define _GNU_SOURCE
#include <time.h>


const char *homedir;

#include "misc.h"

#define CONFIG_FILE_NAME ".config/i3-timetracker/config"

//Current task
int id;
char* title;
int timeSpent;
int suspended;

struct TConfig Config;
sqlite3 *db;

struct SQLResult
{
	//num_rows
	int num_rows;
	int num_cols;
	
	//title of columns 
	char** col_name;
	
	//table
	char*** row;
	
};

void LoadConfigFile(struct TConfig *Config)
{
	FILE *fp;
	char *filename = malloc(sizeof(char) * (strlen(CONFIG_FILE_NAME) + strlen(homedir) + 1));
	sprintf(filename,"%s/%s",homedir,CONFIG_FILE_NAME);

	if ((fp = fopen(filename, "r")) == NULL)
	{
		printf("\nFailed to open config file %s (error %d - %s).\nPlease check that it exists and has read permission.\n", filename, errno, strerror(errno));
		exit(1);
	}
	
	ReadString(fp, "pathToDatabase", -1, &(Config->PathDatabase), 1);
	
	//relative path
	if(Config->PathDatabase[0]!='/'){
		char *Tmp = malloc(sizeof(char) * (strlen(Config->PathDatabase) + strlen(homedir) + 1));
		sprintf(Tmp,"%s/%s",homedir,Config->PathDatabase);
		free(Config->PathDatabase);
		Config->PathDatabase = Tmp;
	}
		
	ReadString(fp, "lazyTitle", -1, &(Config->LazyTitle), 1);

	/*int testI;
	double testD;
	int testB;
	
	testI = ReadInteger(fp, "testInt", -1, 0, 0);
	testD = ReadFloat(fp, "testFloat", -1, 0, 0);
	ReadBoolean(fp, "testBool", -1, 0, &testB);
	if(testB) printf("true\n");
	if(!testB) printf("false\n");
	printf("%d\n",testI);
	printf("%f\n",testD);*/
	
	Config->LazyProgramsCount = ReadStringArray(fp,"lazyPrograms",-1,&(Config->LazyPrograms),0);
	Config->LazyWinTitlesCount = ReadStringArray(fp,"lazyWinTitles",-1,&(Config->LazyWinTitles),0);
	Config->SuspendProgramsCount = ReadStringArray(fp,"suspendPrograms",-1,&(Config->SuspendPrograms),0);

	/*printf("[");
	for(int i = 0; i < Config->LazyProgramsCount; ++i){
		if(i!=0) printf(",");
		printf("%s",Config->LazyPrograms[i]);
	}
	printf("]\n");*/
	
	fclose(fp);
}

static int callback(void *ptr, int argc, char **argv, char **azColName) {
	struct SQLResult *result = ptr;
	//first row
	if(argc != result->num_cols){
		result->num_rows = 0;
		result->num_cols = argc;
		result->col_name = malloc(argc * sizeof(char*));
		for(int i = 0; i<argc; i++) {
			result->col_name[i] = malloc(strlen(azColName[i]) * sizeof(char));
			strcpy(result->col_name[i], azColName[i]);
		}
		result->row = malloc(sizeof(char**));
	}else{
		char*** table = realloc(result->row,(result->num_rows + 1) * sizeof(char**));
		if(table == NULL){
			fprintf(stderr,"Error reallocating space.\n");
			return 1;
		}
		result->row = table;
	}
	result->row[result->num_rows] = malloc(argc * sizeof(char*));
	for(int i = 0; i<argc; i++) {
		result->row[result->num_rows][i] = malloc(strlen(argv[i]) * sizeof(char));
		strcpy(result->row[result->num_rows][i], argv[i]);
	}
	++result->num_rows;
	
	return 0;
}

static int callbackGetCurrentTask(void *NotUsed, int argc, char **argv, char **azColName) {
	for(int i = 0; i<argc; i++) {
		if(strcmp(azColName[i],"ID")==0) id = argv[i] ? atoi(argv[i]) : -1;
		if(strcmp(azColName[i],"TITLE")==0){
			title = malloc(strlen(argv[i]) * sizeof(char));
			strcpy(title, argv[i]);
		}
		if(strcmp(azColName[i],"TIME")==0) timeSpent = argv[i] ? atoi(argv[i]) : -1;
		if(strcmp(azColName[i],"SUSPENDED_SINCE")==0) suspended = argv[i] ? atoi(argv[i]) : -1;
	}
	
	return 0;
}

int InitializeSQL(char* filename){
	char *zErrMsg = 0;
	int rc;
	char *sql;

	/* Open database */
	rc = sqlite3_open(filename, &db);

	if( rc ) {
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		return(0);
	}

	/* Create SQL statement */
	sql = "CREATE TABLE IF NOT EXISTS TASKS("  \
			"TITLE           TEXT    NOT NULL," \
			"STARTED            INT     NOT NULL," \
			"STOPPED            INT     DEFAULT 0," \
			"SUSPENDED_SINCE        INT DEFAULT -1," \
			"SUSPENSION_TIME      INT DEFAULT 0);";

	/* Execute SQL statement */
	rc = sqlite3_exec(db, sql, 0, 0, &zErrMsg);

	if( rc != SQLITE_OK ){
	fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	
}

int stopTask(){
	//To do: Give feedback of tasks name and whether it was actually stopped
	char *zErrMsg = 0;
	int rc;
	char sql[300];
	/* Insert SQL statement */
	sprintf(sql, "UPDATE TASKS SET STOPPED=CASE WHEN SUSPENDED_SINCE=-1 THEN %d WHEN SUSPENDED_SINCE=-2 THEN STOPPED ELSE SUSPENDED_SINCE END,SUSPENDED_SINCE=-2 WHERE ROWID = (SELECT ROWID FROM TASKS ORDER BY ROWID DESC LIMIT 1)",time(0));

	rc = sqlite3_exec(db, sql, 0, 0, &zErrMsg);

	if( rc != SQLITE_OK ){
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		return 0;
	}	
	return 1;
}

int startTask(char* title){
	//To do: check whether task with same name is already running..
	char *zErrMsg = 0;
	int rc;
	char sql[300];
	
	//Stop previous task
	stopTask();
	
	/* Insert SQL statement */
	sprintf(sql, "INSERT INTO TASKS(TITLE, STARTED) VALUES('%s', %d);", title, time(0));

	rc = sqlite3_exec(db, sql, 0, 0, &zErrMsg);

	if( rc != SQLITE_OK ){
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		return 0;
	}	
	return 1;
}

int suspendTask(){
	//To do: Give feedback of tasks name and whether it was actually suspended
	char *zErrMsg = 0;
	int rc;
	char sql[300];
	/* Insert SQL statement */
	sprintf(sql, "UPDATE TASKS SET SUSPENDED_SINCE=%d WHERE SUSPENDED_SINCE = -1 AND ROWID = (SELECT ROWID FROM TASKS ORDER BY ROWID DESC LIMIT 1)",time(0));

	rc = sqlite3_exec(db, sql, 0, 0, &zErrMsg);

	if( rc != SQLITE_OK ){
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		return 0;
	}	
	return 1;
}

int resumeTask(){
	//To do: Give feedback of tasks name and whether it was actually resumed
	char *zErrMsg = 0;
	int rc;
	char sql[300];
	/* Insert SQL statement */
	sprintf(sql, "UPDATE TASKS SET SUSPENSION_TIME = SUSPENSION_TIME + %d-SUSPENDED_SINCE, SUSPENDED_SINCE=-1 WHERE SUSPENDED_SINCE > 0 AND ROWID = (SELECT ROWID FROM TASKS ORDER BY ROWID DESC LIMIT 1)",time(0));

	rc = sqlite3_exec(db, sql, 0, 0, &zErrMsg);

	if( rc != SQLITE_OK ){
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		return 0;
	}	
	return 1;
}

void getCurrentTask(){
	char *zErrMsg = 0;
	int rc;
	char sql[300];
	/* Insert SQL statement */
	sprintf(sql, "SELECT ROWID AS ID,TITLE, CASE WHEN SUSPENDED_SINCE < 0 THEN %d - STARTED - SUSPENSION_TIME ELSE SUSPENDED_SINCE - SUSPENSION_TIME-STARTED END TIME, SUSPENDED_SINCE FROM TASKS ORDER BY ROWID DESC LIMIT 1",time(0));

	rc = sqlite3_exec(db, sql, callbackGetCurrentTask, 0, &zErrMsg);

	if( rc != SQLITE_OK ){
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}	
}

void printCurrentTask(){
	getCurrentTask();
	//task stopped
	if(suspended == -2) printf("(no task)");
	//task running
	else{
		printf("%s (",title);
		printTime(timeSpent);
		if(suspended > 0) printf(", suspended");
		printf(")");
	}
}

void whereClause(char *str,time_t begin, int duration){
	if(begin == 0 || duration == 0){
		sprintf(str,"");
	}else{
		sprintf(str,"WHERE STARTED > %d AND STARTED < %d",begin,begin+duration);
	}
}

void printStats(time_t begin, int duration){
	char *zErrMsg = 0;
	int rc;
	char sql[500];
	char where[100];
	whereClause(where,begin,duration);
	struct SQLResult result;
	result.num_rows = 0;
	/* Insert SQL statement */
	sprintf(sql, "SELECT TITLE, SUM(CASE WHEN SUSPENDED_SINCE = -1 THEN %d - STARTED - SUSPENSION_TIME WHEN SUSPENDED_SINCE = -2 THEN STOPPED - SUSPENSION_TIME-STARTED ELSE SUSPENDED_SINCE - SUSPENSION_TIME-STARTED END) AS TIME, SUM(SUSPENSION_TIME) FROM TASKS %s GROUP BY TITLE",time(0), where);
	
	rc = sqlite3_exec(db, sql, callback, &result, &zErrMsg);
	if(result.num_rows == 0){
		printf("(no activity)\n");
		return;
	}
	
	for(int i = 0; i < result.num_rows; ++i){
		printf("%s (",result.row[i][0]);
		
		printTime(atoi(result.row[i][1]));
		if(atoi(result.row[i][2])){
			printf(", suspended for ");
			printTime(atoi(result.row[i][2]));
		}
		printf(")\n");
	}
	
	if( rc != SQLITE_OK ){
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}
}

void printLog(time_t begin, int duration){
	char *zErrMsg = 0;
	int rc;
	char sql[500];
	char where[100];
	whereClause(where,begin,duration);
	struct SQLResult result;
	result.num_rows = 0;
	/* Insert SQL statement */
	sprintf(sql, "SELECT ROWID,TITLE, STARTED, CASE WHEN SUSPENDED_SINCE = -1 THEN %d - STARTED - SUSPENSION_TIME WHEN SUSPENDED_SINCE = -2 THEN STOPPED - SUSPENSION_TIME-STARTED ELSE SUSPENDED_SINCE - SUSPENSION_TIME-STARTED END TIME, SUSPENDED_SINCE, SUSPENSION_TIME FROM TASKS %s ORDER BY ROWID ASC",time(0),where);
	
	//printf("%s\n",sql);
	
	rc = sqlite3_exec(db, sql, callback, &result, &zErrMsg);
	
	if(result.num_rows == 0){
		printf("(no activity)\n");
		return;
	}
	
	/*for(int i = 0; i < result.num_cols; ++i){
		printf("%s|",result.col_name[i]);
	}
	printf("\n-");
	for(int i = 0; i < result.num_rows; ++i){
		for(int j = 0; j < result.num_cols; ++j)
			printf("%s|",result.row[i][j]);
		printf("\n-");
	}*/
	
	for(int i = 0; i < result.num_rows; ++i){
		char start[20];
		time_t startTime = atoi(result.row[i][2]);
		strftime(start, 20, "%Y-%m-%d %H:%M:%S", localtime(&startTime));
		printf("%s %s(",start,result.row[i][1]);
		printTime(atoi(result.row[i][3]));
		if(atoi(result.row[i][5])){
			printf(", suspended for ");
			printTime(atoi(result.row[i][5]));
		}
		printf(")");
		if(atoi(result.row[i][4]) > 0) printf(" - suspended");
		if(atoi(result.row[i][4]) == -1) printf(" - in progress");
		printf("\n");
	}
	
	if( rc != SQLITE_OK ){
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}	
}

int main(int argc, char* argv[]) {
	
	//Find home directory
	findHomeDir(&homedir);

	
	LoadConfigFile(&Config);
	
	InitializeSQL(Config.PathDatabase);
	
	//Select current task
	if(argc == 1){
		getCurrentTask();
		for(int i = 0; i < Config.LazyProgramsCount; ++i){
			if(prog_count(Config.LazyPrograms[i])){
				if(strcmp(Config.LazyTitle,title) || suspended == -2){
					startTask(Config.LazyTitle);
				}
			}
		}
		for(int i = 0; i < Config.LazyWinTitlesCount; ++i){
			if(window_from_name(Config.LazyWinTitles[i]) != 0){
				if(strcmp(Config.LazyTitle,title) || suspended == -2){
					startTask(Config.LazyTitle);
				}
			}
		}
		if(Config.SuspendProgramsCount){
			int noSuspenderRunning = 1;
			for(int i = 0; i < Config.SuspendProgramsCount; ++i){
				if(prog_count(Config.SuspendPrograms[i])){
					if(suspended == -1) suspendTask();
					noSuspenderRunning = 0;
				}
			}
			if(noSuspenderRunning && suspended > 0) resumeTask();
		}
		printCurrentTask();
	}else{
		if(strcmp(argv[1],"start") == 0){
			//start new task
			if(argc < 3){
				fprintf(stderr,"Error: Too few arguments.\nStart new task with %s start <title>\n",argv[0]);
				exit(1);
			}
			if(startTask(argv[2]))
				printf("%s started.", argv[2]);
			else
				fprintf(stderr,"%s could not be started.\n", argv[2]);
		}
		if(strcmp(argv[1],"stop") == 0){
			//stop task
			stopTask();
		}
		if(strcmp(argv[1],"suspend") == 0){
			//suspend task
			suspendTask();
		}
		if(strcmp(argv[1],"resume") == 0){
			//resume task	
			resumeTask();
		}
		//prints info without automatically detecting started programs
		if(strcmp(argv[1],"info") == 0){
			printCurrentTask();
		}
		//prints stats
		if(strcmp(argv[1],"stats") == 0 || strcmp(argv[1],"log") == 0){
			time_t begin = 0;
			int duration = 0;
			if(argc > 2){
				struct tm tm;
				if(strcmp(argv[2],"today")==0 || strcmp(argv[2],"yesterday")==0){
					begin = time(NULL);
					struct tm *t;
					t = localtime(&begin);
					begin -= t->tm_sec + 60 * t->tm_min + 3600 * t->tm_hour;
					if(strcmp(argv[2],"yesterday")==0){
						begin -= 24*3600;
					}
				}
				else if ( strptime(argv[2], "%Y-%m-%d", &tm) != NULL ){
					/*Without this those variables are undefined and we get random behaviour*/
					tm.tm_sec = 0;
					tm.tm_min = 0;
					tm.tm_hour = 0;
					begin = mktime(&tm);
				}else{
					fprintf(stderr,"invalid time format: %s. YYYY-MM-DD required", argv[2]);
					exit(1);
				}
				duration = 3600*24;
				if(argc > 3){
					duration = atoi(argv[3]);
				}
			}
			if(strcmp(argv[1],"stats") == 0) printStats(begin,duration);
			if(strcmp(argv[1],"log") == 0) printLog(begin,duration);
			
		}
		
	}
	
	sqlite3_close(db);
	return 0;
}
