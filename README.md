# i3-timetracker
## About
i3-timetracker is a simple tool for time tracking. In particular, one can name multiple applications(by command or window title) that start a "procrastinating" time. So, once you are lazy, it shows you how long you have been lazy for.
## Syntax
- i3-timetracker
    - shows current task and duration
- i3-timetracker start <title of new task> 
- i3-timetracker stop|suspend|resume
    - a stopped task cannot be resumed
- i3-timetracker log|stats
    -  log shows all tasks in order of time
    -  stats sums up tasks with the same name
- i3-timetracker log|stats today|yesterday
- i3-timetracker log|stats <date in YYYY-MM-DD> <duration in sec, default is one day>
## Installation
Compile C code with
```
$ gcc i3-timetracker.c -l sqlite3 misc.c -lX11 -o i3-timetracker
```
Create directory for config
```
$ mkdir ~/.config/i3-timetracker
$ cp config ~/.config/i3-timetracker/config
```
Copy binary to /usr/bin and make it executable for everyone
```
$ sudo cp ./i3-timetracker /usr/bin/i3-timetracker
$ sudo chmod a+x /usr/bin/i3-timetracker
```
### Adding i3-timetracker to i3status(assuming no other i3status wrapper is in use so far)
```
$ mkdir ~/.config/i3status/contrib
$ cp ./wrapper.py ~/.config/i3status/contrib/wrapper.py
```
To use it, ensure your ~/.config/i3status/config contains this line:
```
     output_format = "i3bar"
```
in the 'general' section.
Then, in your ~/.i3/config, use:
```
     status_command i3status | ~/.config/i3status/contrib/wrapper.py
```
in the 'bar' section.

### Creating shortcuts
Recommended shortcuts for starting and stopping tasks are(but since it is i3, do whatever you like) - add this to your i3 config file if you want:
```
     #i3-timetracker shortcuts
    bindsym $mod+t exec ~/.config/i3/startTaskDialog.sh
    bindsym $mod+Shift+t exec i3-timetracker stop
```
For this to work write the following to ~/.config/i3/startTaskDialog.sh
```
i3-input -F 'exec i3-timetracker start "%s"' -P 'Start task: '
```
and make it executable
```
$ chmod +x ~/.config/i3/startTaskDialog.sh
```
