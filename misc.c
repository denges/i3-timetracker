#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
// #include <linux/i2c-dev.h>
// #include <fcntl.h>
#include <string.h>
// #include <sys/ioctl.h>
#include <sys/stat.h>
// #include <unistd.h>
// #include <sys/ipc.h>
// #include <sys/shm.h>
// #include <errno.h>
// #include <sys/types.h>
#include <dirent.h>
// #include <stdarg.h>
// #include <ctype.h>
// #include <inttypes.h>

//For finding home dir
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

#include <stddef.h>
// #include <stdio.h>
// #include <stdlib.h>
// #include <string.h>
#include <unistd.h>

#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>




void findHomeDir(const char **homeDir){
	if ((*homeDir = getenv("HOME")) == NULL) {
		*homeDir = getpwuid(getuid())->pw_dir;
	}	
}


int FileExists(char *filename)
{
	struct stat st;

	return stat(filename, &st) == 0;
}




void ReadString(FILE *fp, char *keyword, int Channel, char **Result, int NeedValue)
{
	char line[1024], FullKeyWord[64], *token, *value;
 
	
	if (Channel >= 0)
	{
		sprintf(FullKeyWord, "%s_%d", keyword, Channel);
	}
	else
	{
		strcpy(FullKeyWord, keyword);
	}
 
	fseek(fp, 0, SEEK_SET);
	//*Result = '\0';

	
	while (fgets(line, sizeof(line), fp) != NULL)
	{
		line[strcspn(line, "\r")] = '\0';			// Ignore any CR (in case someone has edited the file from Windows with notepad)
		
		if (*line)
		{
			token = strtok(line, "=");
			if (strcasecmp(FullKeyWord, token) == 0)
			{
				value = strtok(NULL, "\n");
				if(value){
					int Length = strlen(value);
					*Result = (char*) malloc((Length + 1) * sizeof(char));
					strcpy(*Result, value);
					if (Length) (*Result)[Length] = '\0';
				}else *Result = calloc(1,sizeof(char));
				return;
			}
		}
	}

	*Result = (char*) calloc(1, sizeof(char));
	
	if (NeedValue)
	{
		printf("Missing value for '%s' in configuration file\n", keyword);
		exit(1);
	}
}

double ReadFloat(FILE *fp, char *keyword, int Channel, int NeedValue, double DefaultValue)
{
	char* Temp;
	
	ReadString(fp, keyword, Channel, &Temp, NeedValue);

	double value = DefaultValue;
	
	if (Temp[0])
	{
		value = atof(Temp);
	}
	
	free(Temp);
	
	return value;
}

int ReadInteger(FILE *fp, char *keyword, int Channel, int NeedValue, int DefaultValue)
{
	char* Temp;
	
	ReadString(fp, keyword, Channel, &Temp, NeedValue);

	int value = DefaultValue;
	
	if (Temp[0])
	{
		value = atoi(Temp);
	}
	
	free(Temp);
	
	return value;
}

int ReadBoolean(FILE *fp, char *keyword, int Channel, int NeedValue, int *Result)
{
	char* Temp;

	ReadString(fp, keyword, Channel, &Temp, NeedValue);

	if (*Temp)
	{
		*Result = (*Temp == '1') || (*Temp == 'Y') || (*Temp == 'y') || (*Temp == 't') || (*Temp == 'T');
	}
	else
	{
		*Result = 0;
	}
	
	free(Temp);
	
	return 0;
}

int ReadBooleanFromString(FILE *fp, char *keyword, char *searchword)
{
	char* Temp;

	ReadString(fp, keyword, -1, &Temp, 0);
	
	int Return;
	
	if (strcasestr(Temp, searchword)) Return=1; else Return=0;
	
	free(Temp);
	
	return Return;
}

int ReadStringArray(FILE *fp, char *keyword, int Channel, char ***array, int NeedValue){
	char* Temp;
	ReadString(fp, keyword, Channel, &Temp, NeedValue);
	
	char delimiters[] = ",;";
	
	int items = 0;
	
	if(Temp[0] && strcmp(Temp,"")){
		++items;
		for(int i = 0; i < strlen(Temp); ++i){
			for(int j = 0; j < strlen(delimiters); ++j)
				if(Temp[i] == delimiters[j]) ++items;
		}
		char *p = strtok (Temp, delimiters);
		*array = malloc(items * sizeof(char*));
		int i = 0;
		while(p != NULL){
			int Length = strlen(p);
			(*array)[i] = (char*) malloc(Length * sizeof(char));
 			strcpy((*array)[i], p);
			++i;
			p = strtok (NULL, delimiters);
		}
	}
	
	free(Temp);
	return items;
}

int prog_count(char* name)
{
    DIR* dir;
    struct dirent* ent;
    char buf[512];
    long  pid;
    char pname[100] = {0,};
    char state;
    FILE *fp=NULL; 
	int Count=0;

    if (!(dir = opendir("/proc")))
	{
        perror("can't open /proc");
        return 0;
    }

    while((ent = readdir(dir)) != NULL)
	{
        long lpid = atol(ent->d_name);
        if (lpid < 0)
            continue;
        snprintf(buf, sizeof(buf), "/proc/%ld/stat", lpid);
        fp = fopen(buf, "r");

        if (fp)
		{
            if ((fscanf(fp, "%ld (%[^)]) %c", &pid, pname, &state)) != 3 )
			{
                printf("fscanf failed \n");
                fclose(fp);
                closedir(dir);
                return 0;
            }
			
            if (!strcmp(pname, name))
			{
                Count++;
            }
            fclose(fp);
        }
    }

	closedir(dir);
	
	return Count;
}

void printTime(int seconds){
	int hours = seconds / 3600;
	int minutes = (seconds / 60) % 60;
	seconds = seconds % 60;
	printf("%02d:%02d:%02d",hours,minutes,seconds);
}

Window window_from_name_search(Display *display, Window current, char const *needle) {
  Window retval, root, parent, *children;
  unsigned children_count;
  char *name = NULL;

  Atom netWmName;
  Atom utf8;
  Atom actType;
  int actFormat;
//   int nItems;
  unsigned long nItems, bytes;

  char* data;
  
  netWmName = XInternAtom(display, "_NET_WM_NAME", False);
  utf8 = XInternAtom(display, "UTF8_STRING", False);

  XGetWindowProperty(display, current, netWmName, 0, 0x77777777, False, utf8, &actType, &actFormat, &nItems, &bytes, (unsigned char **) &data);
  /* Check if this window has the title we seek */
  if(data != NULL){
	  char* test = strstr(data,needle);
	  XFree(data);	
	  if(test){
		return current;
		  
	}
  }
  
  

  retval = 0;

  /* If it does not: check all subwindows recursively. */
  if(0 != XQueryTree(display, current, &root, &parent, &children, &children_count)) {
    unsigned i;
    for(i = 0; i < children_count; ++i) {
      Window win = window_from_name_search(display, children[i], needle);

      if(win != 0) {
        retval = win;
        break;
      }
    }

    XFree(children);
  }

  return retval;
}

// frontend function: open display connection, start searching from the root window.
Window window_from_name(char const *name) {
  Display *display = XOpenDisplay(NULL);
  Window w = window_from_name_search(display, XDefaultRootWindow(display), name);
  XCloseDisplay(display);
  return w;
}
