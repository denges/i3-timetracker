#include <stddef.h>
#include <unistd.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>

struct TConfig
{
	// filename of SQL Lite db
	char* PathDatabase;
	
	// lazy programs(e.g. games)
	char** LazyPrograms;
	int LazyProgramsCount;
	char** LazyWinTitles;
	int LazyWinTitlesCount;
	char* LazyTitle;
	
	// suspend programs(e.g. i3lock)
	char** SuspendPrograms;
	int SuspendProgramsCount;
	
};

extern struct TConfig Config;

void findHomeDir(const char **homeDir);
int FileExists(char *filename);
void ReadString(FILE *fp, char *keyword, int Channel, char **Result, int NeedValue);
double ReadFloat(FILE *fp, char *keyword, int Channel, int NeedValue, double DefaultValue);
int ReadInteger(FILE *fp, char *keyword, int Channel, int NeedValue, int DefaultValue);
int ReadBoolean(FILE *fp, char *keyword, int Channel, int NeedValue, int *Result);
int ReadBooleanFromString(FILE *fp, char *keyword, char *searchword);
int ReadStringArray(FILE *fp, char *keyword, int Channel, char ***array, int NeedValue);
int prog_count(char* name);
void printTime(int seconds);
Window window_from_name_search(Display *display, Window current, char const *needle);
Window window_from_name(char const *name);
